/**
 * Created by TBinder on 09/06/16.
 */

// window.onload = start();

function start() {

    if (window.performance.now) {
        console.log("Using high performance timer");
        getTimestamp = function () {
            return window.performance.now();
        };
    } else {
        if (window.performance.webkitNow) {
            console.log("Using webkit high performance timer");
            getTimestamp = function () {
                return window.performance.webkitNow();
            };
        } else {
            console.log("Using low performance timer");
            getTimestamp = function () {
                return new Date().getTime();
            };
        }
    }

    var start = getTimestamp();

    var bpm = 80;
    var ms = 60000 / bpm;

    var nexTime = start;

    var running = true;

    var counter = 1;

    var sound = document.createElement('audio');
    sound.setAttribute('src', "res/sound.wav");
    sound.setAttribute('id', 'tick');
    document.body.appendChild(sound);

    while (running) {
        nexTime += ms;
        waitAndPlay(nexTime);

        currentTime = getTimestamp();
        var delta = currentTime - nexTime;

        window.console.log(counter + ": " + delta + "\t\t| delta");
        window.console.log(counter + ": " + nexTime + "\t| nexTime");
        window.console.log(counter + ": " + currentTime + "\t| current time");

        running = !(++counter > 3);
    }
}

function waitAndPlay(time) {
    while (getTimestamp() < time) {

    }

    document.getElementById("tick").play();
    window.console.log("tick");
}